import React, { Component } from 'react';
import PropTypes from 'prop-types';

const ActiveLocationData = ({ selectedState, activeProperty }) => {
    const rows = Object.keys(selectedState).map(function(key) {
        return <tr key={ key } className="">
                    <td><b>{ key }</b></td>
                    <td>{ selectedState[key] }</td>
                </tr> 
    });

    const country = selectedState.country == "US" ? "United States" : console.log('it is not us') 

    return(
        <div className="selected-location">
            <h4 className="selected-location__header p-4 bg-light"> 
                <img className="selected-location__header-icon" style={{ width:"15px" }} src="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.1.0/flags/1x1/us.svg" />
                { selectedState.city }
             </h4>

            <table className="selected-location__table table mb-0">
                <thead>
                    <tr>
                        <th>Criteria</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
                    {rows}
                </tbody>
            </table>
        </div>
    )
}

export default ActiveLocationData;
