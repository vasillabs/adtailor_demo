import React, { Component } from 'react';

import Component_A from './component_a'
import Component_B from './component_b'
import Component_C from './component_c'

const ContentPlaceholder = (props) => {
    let componentToLoad
    switch (props.content) {
        case 'landing_page':
            console.log(props.content);
            break;

        case 'blog':
            console.log(props.content);
            break;

        case 'banner':
            console.log(props.content);
            break;
    
        default:
            break;
    }
    
    return (
        
        <div className="container">
                <div className="jumbotron jumbotron-fluid border rounded p-4">
                    <div className="container-fluid">
                        <h1 className="display-4">{props.content}</h1>
                        <p className="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
                    </div>
                </div>
        </div>
    )
}

export default ContentPlaceholder;