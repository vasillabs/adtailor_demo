import React, { Component } from 'react';

class NavBar extends Component {
    constructor(props) {
        super(props)

        this.state = {
            links: [
                {
                    id: 1,
                    title: "Landing page"
                },
                {
                    id: 2,
                    title: "Blog"
                },
                {
                    id: 3,
                    title: "Banner"
                },
                {
                    id: 4,
                    title: "Component"
                }
            ]
        }

        this.handleNavClick = this.handleNavClick.bind(this)
    }

    handleNavClick(event) {
        this.props.onNavSelect(event.target.dataset.target);              
        document.querySelectorAll('.nav-link').forEach( link => {
            link.classList.remove("alert", "alert-primary") 
        })

        event.target.classList.add("alert", "alert-primary");
    }

    render() {
        const linkItems = this.state.links.map( link => {
            let slugTitle = link.title.toLocaleLowerCase().replace(' ', '_');

            return  <li 
                        key={ link.id }
                        className="top-bar__item nav-item p-3">
                        <a 
                            onClick={ this.handleNavClick } 
                            data-target={ slugTitle } 
                            className="top-bar__link nav-link disabled" 
                            href="#">{ link.title }</a>  
                    </li>
        })

        return (
            <ul className="top-bar nav">
                { linkItems }
            </ul>        
        )
    }
}

export default NavBar;