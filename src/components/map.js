import React, { Component } from 'react';

class LMap extends Component {
        constructor(props) {
            super(props) 

            this.options = {
                apiKey : 'pk.eyJ1IjoidmFzaWxocmlzdG9mZiIsImEiOiJjaWp4dXd3Z3MxYzZqdmJsemo0aGM0MDg4In0.tdnsJBxxRwlkF1s42xx4MQ',
                lat : -92.1047804,
                lng : 38.6617125,
                zoom : 3
            }

            this.state = {
                locationsOnMap: [],
                activeID: null
            }

            this.onPinClickHandle = this.onPinClickHandle.bind(this)
        }
        
        addLocation(location) {
            const marker = L.marker([location.longtitude, location.latitude]),
                  city = location.city,
                  id = location.id,
                  opened = true,
                  iconStyle = "font-size:15px;font-family:Quicksand,sans-serif;font-weight:700";
              
            marker.bindPopup(`<h3 style=${iconStyle} data-attribute={{ ${id}}><i class="ion-person" style="padding-right:.5em;font-size:17px;"></i> ${city}</h3>
                              <i style="display:none" class="state_id">${id }</i>`)

            marker.addTo(this.map)     
            var newLocation = {
                id: id,
                marker: marker                
            }
           
            this.setState(prevState => ({
              locationsOnMap: [...prevState.locationsOnMap, newLocation] 
            }));

            return marker;
        }

        initMap() {
            this.map = L.map(document.querySelector('#map')).setView([this.options.lng, this.options.lat], this.options.zoom);
            
            L.tileLayer(`https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=${this.options.apiKey}`, {
                maxZoom: 15,
                id: 'mapbox.satellite'
            }).addTo(this.map);


            for (const location in this.props.locations) {
                if (this.props.locations.hasOwnProperty(location)) {
                    let locationOnMap = this.addLocation(this.props.locations[location]);
                }
            }
        }

        componentDidMount() {
            this.initMap();
        }

        onPinClickHandle(e) {
          var activeLat = e.popup._latlng.lat
          var activeLng = e.popup._latlng.lng

          var filtered = this.state.locationsOnMap.filter(function (location) {            
            return location.marker._popup._source._latlng.lat == activeLat
          })

          this.props.onLocationSelect(filtered[0].id)
        }

        handlePinClick(locations, currentState) {
            if (this.map) {
                this.map.on('popupopen', this.onPinClickHandle);
            }
        }

        render() {
            return (
                <div id="map">Loading...</div>
            )
        }
}

export default LMap;
