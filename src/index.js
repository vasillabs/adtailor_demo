import _ from 'lodash';
import React, { Component } from "react";
import ReactDOM from "react-dom";

import NavBar from './components/navbar';
import ActiveLocationData from './components/active_location_data';
import LMap from './components/map';
import ContentPlaceholder from './components/content_placeholder';

class App extends Component {
  constructor() {
    super();
    
    this.state = {
      activeContent: '1',
      locations: {},
      selectedLocation: null
    };
  }

  componentDidMount() {
    this.loadLocations()
  }

  loadLocations() {
    const url = '../db.json'
      fetch(url)
        .then((res) => res.json())
        .then((locations) => {
          this.setState({ 
            locations: locations,
            selectedLocation: locations[0] 
          })
        })
  }
    
    render() {
        if(_.isEmpty(this.state.locations)) {
            return (
              <div>Loading data...</div>
            )
        } 

        return (
          <div className="container-fluid">
              <div className="row">
                <div className="col-sm-4 pl-0 left-side">
                    <ActiveLocationData selectedState = { this.state.selectedLocation } />
                    <LMap locations = { this.state.locations } onLocationSelect = { id => {  
                            this.setState({ selectedLocation: this.state.locations[id] })
                          }} />
                </div>

                <div className="col-sm-8 right-side pl-0">
                    <NavBar onNavSelect = { activeContent => this.setState({ activeContent }) } />
                    <ContentPlaceholder location={this.state.selectedLocation} content = { this.state.activeContent } />
                </div>
              </div>
          </div>
        );
  }
}
ReactDOM.render(<App />, document.querySelector(".root"));

  
// ----------------------------------------------------
// |  Adtailor DEMO Schema TODO:                             |
// ----------------------------------------------------
/*  
      DONE :: 1) Refactor and structure code
      DONE :: 2) Add SASS module
      DONE :: 3) Refactor Map class with Leaflet React components
      4) Bootstrap and  BEM structure
      5) Integrate simple model with Redux  
      6) Transfer old structure
      7) Improve design
    */
